#include <iostream>
#include <string>

void Game();
void GameBoard(char board[3][3]);
int Move(char symbol, int plays, int Moves[9]);
bool check(char board[3][3], char symbol, int plays);


int main()
{
	using namespace std;

	cout << "Tic Tac Toe !!! \n"
		<< "____________________\n\n";

	Game();

	char yn = 'a';

	//Check if players want to play another game
	while (true)
	{
		while (tolower(yn) != 'y' && tolower(yn) != 'n')
		{
			cout << "Do you want to play again? (y/n): ";
			cin >> yn;

			//incorrect inputs
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
		}

		if (tolower(yn) == 'y')
			Game();
		else
			break;
	}

	return 0;
}

//Sets up board and runs 9 plays
void Game()
{
	using namespace std;

	char board[3][3] =
	{
		{ ' ', ' ', ' ', },
	{ ' ', ' ', ' ', },
	{ ' ', ' ', ' ' }
	};
	GameBoard(board);

	char symbol = ' ';
	int move = 0;
	int Moves[9] = { 0 };

	for (int plays = 1; plays < 10; plays++)
	{
		if (!(plays % 2))
			symbol = 'O';
		else
			symbol = 'X';

		move = Move(symbol, plays, Moves);
		Moves[plays - 1] = move;

		int x = 0, y = 0;
		x = (move + 2) % 3;
		y = (move - 1) / 3; 
		board[x][y] = symbol;

		GameBoard(board);

		if (plays > 4)
			if (check(board, symbol, plays)) break;
	}
}

//Prints the board
void GameBoard(char board[3][3])
{
	using namespace std;

	string sLine = "";

	cout << endl;

	for (int k = 0; k < 3; k++)
	{
		sLine = "";

		for (int l = 0; l < 3; l++)
		{
			sLine += board[l][k];
			if (l < 2) sLine += "|";
		}

		cout << sLine << endl;
		if (k < 2) cout << "-----" << endl;
	}

	cout << endl;
}
//move and checks for errors
int Move(char symbol, int plays, int Moves[9])
{
	using namespace std;

	bool validPlay = false;
	int move = 0;

	while (!validPlay)
	{
		while (move < 1 || move > 9)
		{
			cout << "Player " << symbol << " enter your move: ";
			cin >> move;

			if (!cin)
			{
				cin.clear();
				cin.ignore(numeric_limits<streamsize>::max(), '\n');
			}
		}

		validPlay = true;

		for (int J = 0; J < plays; J++)
		{
			if (move == Moves[J]) validPlay = false;
		}
	}

	return move;
}

//Check  if  player has won or tie
bool check(char board[3][3], char symbol, int plays)
{
	using namespace std;

	bool winner = false;

	
	for (int J = 0; J < 3; J++)
	{
		if (board[J][0] == symbol && board[J][1] == symbol && board[J][2] == symbol)
			winner = true;
		if (board[0][J] == symbol && board[1][J] == symbol && board[2][J] == symbol)
			winner = true;
	}
	 return true;
}